import { Component, Input } from '@angular/core';
import { ISkill2 } from '../tile/skill.model';
import { SkillsService } from '../skills.service';

@Component({
  selector: 'main-tile',
  templateUrl: './main-tile.component.html',
  styleUrls: ['./main-tile.component.scss'],
})
export class MainTileComponent {

  constructor(private skillsService: SkillsService) {}

  @Input() addedSkill: ISkill2;
  @Input() fromSis: boolean;


  userInput: string;

  clearInput() {
    this.userInput = "";
  }

  rate(ratingValue) {
    this.addedSkill.lvl = ratingValue;
  }

  delete() {
    this.skillsService.deleteFromAdded(this.addedSkill);
  }

}
