import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { SkillsService } from './skills.service';

@Component({
    selector: 'input-powerful',
    templateUrl: './input-powerful.component.html',
    styleUrls: ['./input-powerful.component.scss']
})

export class InputPowerful implements OnChanges { // , OnChanges {
    tmp = '';
    inputValue = '';
    // editMode = false;
    @Input() editMode: boolean;

    constructor(private service: SkillsService) { }

    ngOnChanges() {
        console.log('zmiana!', this.editMode);
        if (this.editMode) {
            this.inputValue = this.service.getSkillsString();
        } else {
            console.log('this.inputValue', this.inputValue);
            this.service.updateSkills(this.inputValue);
            this.inputValue = '';
        }
    }



    doMagic(event) {
        const textarea = event.target;
        this.fitTextareaHeight(textarea);
        this.takeValuesFromTextarea(event); // textarea.value);
    }
    fitTextareaHeight(textarea) {
        textarea.style.height = 0; // has to set to 0 for shrinking
        textarea.style.height = textarea.scrollHeight + 'px';
    }

    takeValuesFromTextarea(event) {
        const code = (event.keyCode ? event.keyCode : event.which);
        const data = event.target.value;
        this.inputValue = data;
        // console.log('data', data);
        // console.log('inputValue', this.inputValue);
        // console.log('keyCode', code); // 188- przecinek, 13 - enter

        if (code === 13) {
            console.log('enter');

            if (!this.editMode) {
                this.service.addSkills(data);
                this.inputValue = '';
            } else {
                this.service.updateSkills(data);
            }
        }
    }
    getProperPlaceholder() {
        return this.editMode
            ? 'What\'s your skills? Add some new! (Remember about pattern!)'
            : 'What\'s your skills? Edit mode. (Remember about pattern!)';
    }
}






// >>>>> Validators.pattern('([a-zA-Z]+(\\s?[\\,\\.\\-]?\\s?[1-5]?)?){1}') <<<<
// We must use double backslashes because the pattern is passed to the regex engine as a string literal,
// and regex escaping requires literal backslashes with \d, \w, etc.
// '(\\s?[a-zA-Z\\+\\#\\-\\$\\!\\*\\@\\.\\s]+\\s+[1-5]{1})' -> string po dwa \\
// '/(\s?[a-zA-Z\+\#\-\$\!\*\@\.\s]+\s+[1-5]{1})/ -> wyrazenie po jeden \

  /*
            wzorzec ->
                technical: operating systems: nana 3, nana 5, mama 3, lsdas2 2, das

        __Technical__
        Operating systems:
        Languages:
        Frameworks:
        Libraries:
        Containers & Servers:
        Databases:
        Testing:
        Version Control Systems:
        Project development and management tools:
        Methodologies:
        Other:

        __Business__:

        */

 // const regExp = /(oper:){1}(\s?\,?(\s?[a-zA-Z]+\s+[1-5]{1}){1})*/; <- tu nie ma obowiazku przecinku, lepiej miec imo
        // const regExp = /(oper:){1}(\s?[a-zA-Z]+\s+[1-5]{1})?(\,{1}(\s?[a-zA-Z]+\s+[1-5]{1}){1})+/; <- tu jest obowiazek przecinkow
        // (oper:){1}(\s?[a-zA-Z]+\s+[1-5]{1})?([\,\;]{1}(\s?[a-zA-Z\+\#\-\$\!\*\@\.\s]+\s+[1-5]{1}){1})*
        // tslint:disable-next-line:max-line-length
        // const regExp = /(oper:){1}(\s?\S+\s+[1-5]{1})?([\,\;]{1}(\s?\S+\s+[1-5]{1}){1})+/; <- tu \S nie wymusza, aby byla spacja miedzy skillem i ratem



// hard option
/*
  const fragment = '(\\s?[a-zA-Z\\+\\#\\-\\$\\!\\*\\@\\.\\s]+\\s+[1-5]{1})';
        const regExp = new RegExp('(oper:){1}' + fragment + '?([\\,\\;]{1}' + fragment + '{1})*');
        const matchData = regExp.exec(data);

        if (matchData) {
             const skills1 = matchData[0].split(':').pop().split('\n').shift();
            // pop() pozbywa sie ostatniego elem tab, a shift() pierwszego. Oba zwracaja to czego sie pozbyly.

             const skillsOper = skills1.split(',');
            if (skillsOper.length !== 0 && skillsOper[0] !== '') {
                this.service.updateTable(skillsOper);
            }
        }



        //oper: java 4, scala 3
*/

// let cc = ['1', '2s', '3'];
// cc = this.duplicate(cc);
// console.log(Array.isArray(cc));
// duplicate(arr: any[]) {
//     let tmp = arr;
//     tmp.push(...arr);
//     console.log('tmp', tmp);
//     console.log('arr', arr);
//     return tmp;
// }
// duplicate2(arr: any[]) {
//     let tmp: Array<any>;
//     tmp = arr.slice();
//     tmp.push(...tmp);
//     console.log('tmp', tmp);
//     console.log('arr', arr);
//     return tmp;
// }

