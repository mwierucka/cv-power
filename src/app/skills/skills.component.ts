import { Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ISkill, ISkill2 } from 'src/app/skills/tile/skill.model';
import { ExampleSkillsService } from '../webapi/exampleSkills.service';
import { SkillsService } from './skills.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit{
  options: FormGroup;
  exampleSkills: ISkill2[];
  data: number = 0;
  name: string;
  editMode: boolean = false;
  pen: boolean = true;
  addedName: string;
  addedSkills: ISkill2[];


  // @Input() name: string;
  // @Input() data: number = 0;

  // TESTUJĘ Z REACTIVE FORMS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  nameValue = new FormControl("", Validators.required);
  // rateValue = new FormControl(0, Validators.required);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
  constructor(formBuilder: FormBuilder, private exampleSkillsService: ExampleSkillsService, private skillsService: SkillsService) {
    this.options = formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
    });
  }

  ngOnInit() {
    this.exampleSkills = this.exampleSkillsService.getExampleSkills();
    this.addedSkills = this.skillsService.getSkills(); // .subscribe(events => {this.events = events});
    console.log(this.addedSkills);
  }
  
  edit() {
    this.editMode = !this.editMode;
    this.pen = !this.pen;
  }

   // TESTUJĘ Z REACTIVE FORMS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // rate(ratingValue, nameValue) {
  //   this.data = ratingValue;
  //   this.name = nameValue;
  //   this.skillsService.addSkillToArray(this.name, this.data);
  //   console.log(this.nameValue)
  // }
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // getAddedSkills(){
  //   return this.skillsService.getSkills();
  // }

    

  rate(rateValue) {
    this.name = this.addedName;
    this.data = rateValue;
    console.log(this.name);
    console.log(this.data)
    this.skillsService.addSkillToArray(this.name, this.data);
}

}
