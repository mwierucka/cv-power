import { Injectable } from "@angular/core";
import { GetCVService } from "../webapi/getCv.service";
import { ISkill, ISkill2 } from "./tile/skill.model";
import { ExampleSkillsService } from "../webapi/exampleSkills.service";

@Injectable({
  providedIn: "root"
})
export class SkillsService {
  skill: ISkill2;
  // addedSkills: ISkill2[] = [];

  constructor(
    private getCvService: GetCVService,
    private exampleSkillsService: ExampleSkillsService
  ) {}

  // getSkills() {
  //   this.getCvService.getCv();
  // }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~P O R Z Ą D E K~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // suggestedSkills: string[];
  // addedSkills: ISkill[] = [];

  // getSkills() {
  //   this.getCvService.getCv();
  // }

  // getExampleSkills() {
  //   this.getExampleSkills.getSuggestions();
  // }

  // addFromSuggestions(name: string, value: number) {
  //   this.addedSkills.push({name: name, value: value})
  // }

  // addFromInput(name: string, value: number) {
  //   this.addedSkills.push({name: name, value: value})                     🤷🤷🤷🤷🤷🤷 czy to musi być oddzielna funkcja?
  // }

  deleteFromAdded(addedSkill) {
    // let index = this.addedSkills.indexOf(addedSkill);
    // this.addedSkills.splice(index, 1);
    let index = SKILLS.indexOf(addedSkill);
    SKILLS.splice(index, 1);
  }

  deleteFromSuggestions(exampleSkill) {
    let array = this.exampleSkillsService.getExampleSkills();
    let index = array.indexOf(exampleSkill);
    array.splice(index, 1);
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  addSkillToArray(name, rate) {
    // this.addedSkills.push({ name: name, value: rate });
    SKILLS.push({ name: name, lvl: rate });
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  addSkill(skill: ISkill2) {
    SKILLS.push(skill);
  }

  removeSkill(skill: ISkill2) {
    const item = SKILLS.find(s => s === skill);
    SKILLS.splice(SKILLS.indexOf(item), 1);
  }

  updateSkills(data: string) {
    const skills = this.splitTextareaValue(data);
    if (skills !== null) {
      this.updateTable(skills, false);
    }
  }

  addSkills(data: string) {
    const skills = this.splitTextareaValue(data);
    if (skills !== null) {
      this.updateTable(skills, true);
    }
  }

  /**
   *
   * @param data string data to split into pieces
   */
  splitTextareaValue(data: string) {
    const fragment =
      "(\\s?[a-zA-Z\\+\\#\\-\\$\\!\\*\\@\\.\\s\\d]+\\s+\\*{1}[1-5]{1})";
    const regExp = new RegExp(fragment + "?([\\,\\;]{1}" + fragment + "{1})*");
    const matchData = regExp.exec(data);

    if (matchData) {
      const skillsOper = matchData[0].split(",");
      return skillsOper.length !== 0 && skillsOper[0] !== ""
        ? skillsOper
        : null;
    }
    return null;
  }

  updateTable(newData, ifAdd) {
    let howMuchToCut = 3;
    if (!ifAdd) {
      this.cleanTab();
      howMuchToCut = 2;
    }
    newData.map(elem => {
      const name = elem.slice(0, elem.length - howMuchToCut).trim();
      const lvl = elem.slice(-1);
      // console.log('name: ' + name + ', lvl: ' + lvl);
      SKILLS.push({ name: name, lvl: lvl });
    });
  }

  getSkillsString() {
    let stringValue = "";
    SKILLS.forEach(elem => {
      stringValue += elem.name + " *" + elem.lvl + ", ";
    });
    return stringValue;
  }
  getSkills() {
    return SKILLS;
  }
  cleanTab() {
    SKILLS.length = 0;
  }
}

let SKILLS: ISkill2[] = [
  // {
  //   name: "Scala",
  //   lvl: 4
  // },
  // {
  //   name: "PHP",
  //   lvl: 5
  // }
];
