import { Component, Input } from '@angular/core';
import { SkillsService } from './skills.service';

@Component({
    selector: 'form-skill-addition-powerfull',
    templateUrl: './form-skill-addition-powerfull.component.html',
    styleUrls: ['./form-skill-addition-powerfull.component.scss']
})

export class FormSkillAdditionPowerfull { // } implements OnInit {
    @Input() editMode;
    // pen: boolean = true;
    constructor(private skillsService: SkillsService) { }
    addedSkills = this.skillsService.getSkills();

    editSkill() {
        console.log('editSkill');
        this.editMode = !this.editMode;
        // this.pen = !this.pen;
    }
}

