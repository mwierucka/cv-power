import { Component, Input } from '@angular/core';
import { SkillsService } from '../skills.service';
import { ISkill2 } from './skill.model';


@Component({
  selector: 'tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent {
  clicked: boolean = false;
  ratingAllowed: boolean = true;
  fadeIn: boolean;
  

  constructor(private skillsService: SkillsService) {}

  @Input() data = 0;
  @Input() name: string;

  @Input() exampleSkill: ISkill2;

  rate(ratingValue) {
    
    this.data = ratingValue;
    setTimeout(() => {
      this.ratingAllowed = false;
      setTimeout(() => {this.skillsService.addSkillToArray(this.name, this.data);
      }, 100);
    }, 100);
    console.log(this.name);
    console.log(this.data)
    
  }

  delete() {
    this.skillsService.deleteFromSuggestions(this.exampleSkill);
  }

  showRating() {
    this.clicked = true;
  }
}
