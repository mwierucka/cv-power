import { TestBed, inject } from '@angular/core/testing';

import { ExampleSkillsService } from './exampleSkills.service';

describe('Webapi\exampleSkillsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExampleSkillsService]
    });
  });

  it('should be created', inject([ExampleSkillsService], (service: ExampleSkillsService) => {
    expect(service).toBeTruthy();
  }));
});
