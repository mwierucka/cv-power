import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExampleSkillsService {

  constructor() { }

  getExampleSkills() {
    return exampleSkills;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~P O R Z Ą D E K~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // getSuggestions() {
  
  // }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}


const exampleSkills = [
  {
    name: 'Java',
    lvl: 0
  },
  {
    name: 'Perl',
    lvl: 0
  },
  {
    name: 'JavaScript',
    lvl: 0
  },
  {
    name: 'C#',
    lvl: 0
  },
  {
    name: 'C++',
    lvl: 0
  },
  {
    name: 'SQL',
    lvl: 0
  },
  {
    name: 'Ruby',
    lvl: 0
  }
  ]